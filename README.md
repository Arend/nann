# Neural LSH
Implementation based on [this paper](https://arxiv.org/pdf/1901.08544.pdf) by Dong et al.

Represent known nearest neighbors in a k-NN graph and partition using [KaHIP](https://github.com/schulzchristian/KaHIP).
Train a neural network to predict the part in which a given point lies in the k-NN graph.
Use this neural network as an index over the k-NN graph, to predict the part in which to look for nearest neighbors for a query.

## Report
[View the latest compiled version of the Master's thesis report](https://gitlab.com/Arend/nann/-/jobs/artifacts/master/file/report/report.pdf?job=compile_pdf)

## Scons, MPI, KaHIP installation notes
KaHIP relies on scons and MPI:

- install scons: `apt-get install scons`
- install mpi (requires compilation):
  - download mpi < 1.8: https://www.open-mpi.org/software/ompi/v1.7/
  - compile:
```
$ gunzip -c openmpi-4.0.0.tar.gz | tar xf -
$ cd openmpi-4.0.0
$ ./configure --prefix=/usr/local # or somewhere else, make sure to add <prefix>/mpi/bin to $PATH
<...lots of output...>
$ make all install
```

- follow the installation notes in the [KaHIP readme](https://github.com/schulzchristian/KaHIP/blob/master/README.md#installation-notes):
  - `./compile.sh`
  - test with `./deploy/kaffpa examples/delaunay_n15.graph --k 2 --preconfiguration=strong`

## Faiss setup
Faiss depends on libomp and libopenblas, according to the instructions with the repo of the [unofficial faiss pip distribution](https://github.com/onfido/faiss_prebuilt/blob/master/README.md), installation on Ubuntu is done using
```
apt install libopenblas-base libomp-dev
```
On Mac OSX:
```
brew install libomp openblas
```

## Build k-NN graph for KaHIP
KaHIP requires the k-NN graph to be entered as an adjacency list as follows:
```
n m
neighbor_1 neighbor_2 .. neighbor_n
```

Here `n` is the number of vertices and `m` the number of undirected edges.
Vertex indexing starts at 1.
Each of a vertex's neighbors is listed as show above.
Note that edges have to be specified symmetrically, e.g. if v1 has an edge to v2, v2 must also have an edge to v1.
This means that there can only be edges to vertices included in the vertex set.

E.g. this is not allowed, because v3 is not in the vertex set:
```
2 2
2 3
1 3
```

Edges are undirected, so even though their are specified both ways, `m` is the total number of adjacent vertices in the adjacency list, divided by 2.
Self edges and parallel edges are not allowed.

A problem with known datasets, is that they provide ground truth nearest neighbors for a subset, where the neighbors of this subset may lie outside the subset. KaHIP seems to not allow such a graph. Current workaround is to calculate nearest neighbors on a subset, only considering potential neighbors inside the same subset (`calc_nearest_neighbors.py`).
```
python calc_nearest_neighbors.py datasets/glove-100-angular.hdf5 neighbors.h5
python build_graph.py neighbors.h5 graph.txt
../KaHIP/deploy/kaffpa --k=[number_of_parts] --preconfiguration=[strong] --output_filename=partition.txt graph.txt
```

## Build dataset
Compiles the dataset for the network to consume, based on the original glove dataset and the partitioning created by KaHIP.
```
python build_dataset.py datasets/glove-100-angular.hfd5 partition.txt dataset.h5
```
## Model
See `python model.py --help`
