.PHONY: default
default:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$' | xargs

.PHONY: clean
clean:
	rm -rf `find . -type d -name .mypy_cache -o -name .pytest_cache -o -name htmlcov -o -name __pycache__ -o -name *.egg-info`
	rm -f `find . -name *.pyc`
	rm -f `find report -name "*.aux" \
	-o -name "*.log" \
	-o -name "*.out" \
	-o -name "*.fdb_latexmk" \
	-o -name "*.fls" \
	-o -name "*.toc" \
	-o -name "*.dvi" \
	-o -name "*.acn" \
	-o -name "*.acr" \
	-o -name "*.alg" \
	-o -name "*.glg" \
	-o -name "*.glo" \
	-o -name "*.gls" \
	-o -name "*.ist" \
	-o -name "dictionary.dic"`


.PHONY: clear-nann-cache
clear-nann-cache:
	rm -rf `find . -type d __nann_cache__`

.PHONY: docker-build
docker-build: clean
	docker build -t nann/dataset:latest -f Dockerfile.dataset . && \
	docker build -t nann/model:latest -f Dockerfile.model . && \
	docker build -t nann/nann:latest -f Dockerfile.nann .

.PHONY: docker-index-test
docker-index-test: clean
	docker build -f integration_test/Dockerfile .

.PHONY: docker-test
docker-test: clean
	docker build .

.PHONY: lint
lint:
	black .

.PHONY: lint-check
lint-check:
	black --check .

.PHONY: local-index-test
local-index-test:
	python integration_test/index_test_runner.py

.PHONY: report
report: _report clean

.PHONY: run-all-tests
run-all-tests: clean lint-check spellcheck test-all local-index-test

.PHONY: spellcheck
spellcheck:
	@pyspelling --config spellcheck.yaml \
	&& echo "\033[1;32m"================================================================================"\033[0m" \
	|| (echo "\033[1;31m"================================================================================"\033[0m" ; exit 1)

.PHONY: test
test: lint-check
	pytest -m "not slow" --cov=nann --cov-report=term --cov-report=html tests/

.PHONY: test-all
test-all: lint-check
	pytest --cov=nann --cov-report=term --cov-report=html tests/

.PHONY: watch
watch:
	cd report && latexmk -pvc -pdf -f && cd ..

.PHONY: _report
_report:
	cd report && latexmk -pdf
