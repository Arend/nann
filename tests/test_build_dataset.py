import numpy as np
import pytest

from nann import build_dataset


def _get_random_testset(n, d=25):
    X = np.random.rand(n, d)
    y = np.random.rand(n, 1)
    return X, y


def test_get_subset_takes_subset_of_correct_dimensions():
    n = 100
    d = 23
    X, _ = _get_random_testset(n, d)
    subset_size = 13

    X_train = build_dataset.get_subset(X, subset_size)

    assert subset_size == X_train.shape[0]
    assert d == X_train.shape[1]


def test_get_subset_does_not_shuffle_input_array():
    X, _ = _get_random_testset(100, 12)
    X_copy = np.copy(X)

    X_train = build_dataset.get_subset(X, 3)

    assert (X == X_copy).all()


def test_calc_nearest_neighbors():
    X = np.array([[0.2], [0.3], [0.6]])
    k = 3
    metric = "euclidean"
    expected_nns = np.array([[0, 1, 2], [1, 0, 2], [2, 1, 0]])

    actual_nns = build_dataset.calc_nearest_neighbors(X, k, metric)

    assert (expected_nns == actual_nns).all()


def test_create_knn_graph_creates_kahip_compatible_string():
    # X_train = np.array([[0.2], [0.3], [0.6]])
    nns = np.array([[0, 1, 2], [1, 0, 2], [2, 1, 0]])
    # expected 2 nearest neighbors, start index is 1:
    # self edges not allowed, so vector cannot be its own neighbor.
    # therefore the k-nn graph for k = 3 contains 2 neighbors per vector.
    # 2 3
    # 1 3
    # 2 1

    # kahip expects first line to be:
    # n m
    # where n is the number of vertices
    # and m is the number of edges / 2
    # so in this case:
    # 3 3
    kahip_compatible_string = "3 3\n2 3\n1 3\n2 1"

    graph = build_dataset.create_knn_graph(nns)

    assert kahip_compatible_string == graph


def _slow_contains(X, Y, X_subset, Y_subset):
    all_found = False
    for xx, yy in zip(X_subset, Y_subset):
        found = False
        for x, y in zip(X, Y):
            if np.all(x == xx) and np.all(y == yy):
                found = True
                break
        all_found = found
        if not all_found:
            return False
    return all_found


def test_split_dataset_keeps_pairs_together():
    X, Y = _get_random_testset(10)
    xtr, ytr, xte, yte = build_dataset.split_dataset(X, Y, 0.5)

    assert _slow_contains(X, Y, xtr, ytr)
    assert _slow_contains(X, Y, xte, yte)

    XX = np.concatenate((xtr, xte))
    YY = np.concatenate((ytr, yte))
    assert _slow_contains(X, Y, XX, YY)

    assert len(X) == len(Y)
    assert len(X) == len(xtr) + len(xte)
    assert len(Y) == len(ytr) + len(yte)


def test_split_dataset_does_not_shuffle_input_array():
    X, Y = _get_random_testset(10)
    original_X = np.copy(X)
    original_Y = np.copy(Y)

    xtr, ytr, xte, yte = build_dataset.split_dataset(X, Y, 0.85)

    assert (X == original_X).all()
    assert (Y == original_Y).all()


def test_split_dataset_shuffles_data():
    X, Y = _get_random_testset(10)

    xtr, ytr, xte, yte = build_dataset.split_dataset(X, Y, 0.85)
    shuffled_X = np.concatenate((xtr, xte))
    shuffled_Y = np.concatenate((ytr, yte))

    assert not (X == shuffled_X).all()
    assert not (Y == shuffled_Y).all()


def test_get_partition_distribution():
    partition = np.array([0, 0, 1])
    # 2 nearest neighbors including query vector itself
    nns = np.array([[0, 1], [1, 0], [2, 1]])
    # expected partition distribution includes query vector itself
    # [2, 0] means 2 vectors fall in part 0 and no vectors fall in part 1
    expected_distribution = np.array([[2, 0], [2, 0], [1, 1]])

    actual_distribution = build_dataset.get_partition_distribution(partition, nns)

    assert (expected_distribution == actual_distribution).all()


def test_flatten_partition():
    partition = np.array([2, 0, 0, 1, 2])
    expected_flattened_partition = np.array([1, 2, 3, 0, 4])
    # idxs should contain part starting indices and the end index
    expected_flat_partition_idxs = np.array([0, 2, 3, 5])

    fp, p_idxs = build_dataset.flatten_partition(partition)

    assert (expected_flattened_partition == fp).all()
    assert (expected_flat_partition_idxs == p_idxs).all()


@pytest.mark.parametrize(
    "assymetric, expected_symmetric, expected_m",
    [
        (np.array([[1], [0], [0]]), np.array([[1, 2], [0], [0]]), 2),
        (np.array([[1], [2], [0]]), np.array([[1, 2], [2, 0], [0, 1]]), 3),
    ],
)
def test_symmetric(assymetric, expected_symmetric, expected_m):
    symmetric, m = build_dataset.symmetric(assymetric)

    assert (expected_symmetric == symmetric).any()
    assert expected_m == m
