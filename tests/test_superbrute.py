import pytest
import numpy as np

from nann.superbrute import search_knn


def _query_superbrute():
    X = np.array([[0.1], [0.4], [0.5]]).astype("float32")
    queries = np.array([[0.1], [0.2], [0.6]]).astype("float32")
    _, results = search_knn(queries, X, 2, "euclidean")
    return results


def test_superbrute_batch_query_returns_np_ndarray():
    results = _query_superbrute()
    assert np.ndarray == type(results)


def test_superbrute_batch_query_returns_correct_results():
    expected_results = np.array([[0, 1], [0, 1], [2, 1]])
    results = _query_superbrute()

    assert (expected_results == results).all()


@pytest.mark.parametrize(
    "metric, expected",
    [
        ("euclidean", np.array([[0, 1, 2], [1, 2, 0], [2, 1, 0]])),
        ("angular", np.array([[0, 1, 2], [1, 2, 0], [2, 1, 0]])),
    ],
)
def test_bruteforce_query_multiple_returns_nearest_neighbors(metric, expected):
    X = np.array([[-0.4, -0.4], [1.01, 0.0], [1.0, 0.9]]).astype("float32")

    _, results = search_knn(X, X, 3, metric)
    assert (expected == results).all()
