import numpy as np
import pytest
from unittest.mock import MagicMock

from nann.model import get_model, get_model_names


@pytest.mark.parametrize("model_type", get_model_names())
def test_constructor(model_type):
    m = 2
    pp = get_model(model_type, m)


@pytest.mark.filterwarnings("ignore")
@pytest.mark.parametrize("model_type", get_model_names())
def test_fit(model_type):
    m = 16
    pp = get_model(model_type, m)
    x = np.random.rand(100, 20)
    y = np.random.randint(30, size=(100, m))
    pp.fit(x, y, 1)


@pytest.mark.slow
@pytest.mark.filterwarnings("ignore")
@pytest.mark.parametrize("model_type", get_model_names())
def test_predict_one_returns_distribution(model_type):
    m = 16
    pp = _setup_model_test(model_type, m)

    prediction = pp.predict_one(np.random.rand(20))

    assert prediction.shape == (m,)
    assert np.ndarray == type(prediction)
    assert 1.0 == pytest.approx(np.sum(prediction))
    assert (0 < prediction).all()
    assert (1 > prediction).all()


@pytest.mark.slow
@pytest.mark.filterwarnings("ignore")
@pytest.mark.parametrize("model_type", get_model_names())
def test_predict_multiple(model_type):
    m = 16
    pp = _setup_model_test(model_type, m)

    prediction = pp.predict_multiple(np.random.rand(100, 20))

    assert prediction.shape == (100, m)
    assert np.ndarray == type(prediction)
    assert 1.0 == pytest.approx(np.sum(prediction) / 100)
    assert (0 < prediction).all()
    assert (1 > prediction).all()


@pytest.mark.slow
@pytest.mark.filterwarnings("ignore")
def test_regression_prediction_fits_obvious_dataset():
    m = 3
    n = 100
    maxcount = 30
    pp = get_model("regression", m)
    rs = np.random.RandomState(42)
    X = np.ones((n, 20))
    # half of X has a value of 0.0 on position 1
    X[: n // 2, 1:2] = 0.0
    y = np.random.randint(maxcount // 2, size=(n, m))
    # the corresponding half of y has the max value on position 0
    # (i.e. highest probability to fall in bucket # 0)
    y[: n // 2, :1] = maxcount
    # shuffle X and y with same random seed
    rs.shuffle(X)
    rs.shuffle(y)
    pp.fit(X, y, 1)

    x_test = np.random.rand(20)
    x_test[1] = 0.0

    expected_argmax = 0  # we expect the highest probability for bucket 0

    prediction = pp.predict_one(x_test)

    assert expected_argmax == prediction.argmax()


def _setup_model_test(model_type, m, n=100):
    pp = get_model(model_type, m)
    x = np.random.rand(n, 20)
    y = np.random.randint(30, size=(n - m, m))
    # add an m*m identity matrix to y, to ensure each bucket
    # is represented in the training data for regression lsh
    # (which picks the top bucket only)
    # this should not be a problem in practice,
    # otherwise an exception will be raised
    y = np.concatenate((y, np.identity(m)))
    assert len(np.unique(y.argmax(axis=1))) == m
    pp.fit(x, y, 1)
    return pp
