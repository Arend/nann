import numpy as np
import pytest
from joblib import Memory
from unittest.mock import MagicMock, patch

from nann import evaluate
from nann.index import fit_model
from nann.model import get_model


@patch("joblib.Memory", MagicMock())
def _get_mocked_evaluation(y_test, predictions):
    with patch(
        "nann.evaluate._get_ds",
        MagicMock(return_value=(y_test, y_test, y_test, y_test)),
    ):
        with patch(
            "nann.neural_lsh.NeuralLSH.predict_multiple",
            MagicMock(return_value=predictions),
        ):
            kl_mean, kl_std, queries_per_second = evaluate.evaluate(
                "neural", "dataset_euclidean.filename", 1
            )

    return kl_mean, kl_std, queries_per_second


@pytest.mark.slow
def test_evaluate_gives_error_of_zero_when_distributions_are_identical():
    y_test = np.array([[0, 1, 4], [5, 2, 1]])
    predictions = y_test.astype(np.float64)

    mean_error, stdev, _ = _get_mocked_evaluation(y_test, predictions)

    assert 0 == pytest.approx(mean_error, abs=1e-10)
    assert 0 == pytest.approx(stdev, abs=1e-10)


@pytest.mark.slow
def test_evaluate_gives_error_of_one_when_distributions_have_no_overlap():
    y_test = np.array([[0, 1, 4], [5, 0, 1]])
    predictions = np.array([[5.0, 0.0, 0.0], [0.0, 6.0, 0.0]])

    mean_error, stdev, _ = _get_mocked_evaluation(y_test, predictions)

    assert 1 == pytest.approx(mean_error, abs=1e-10)
    assert 0 == pytest.approx(stdev, abs=1e-10)


@pytest.mark.slow
def test_evaluate_calculates_mean_error_for_each_pair():
    y_test = np.array([[0, 1, 4], [5, 2, 1]])
    predictions = np.array([[0.0, 2.0, 4.0], [5.0, 2.0, 1.0]])

    mean_error_0, _, _ = _get_mocked_evaluation(y_test[0:1], predictions[0:1])
    mean_error_1, _, _ = _get_mocked_evaluation(y_test[1:2], predictions[1:2])
    expected_mean = (mean_error_0 + mean_error_1) / 2
    mean_error, _, _ = _get_mocked_evaluation(y_test, predictions)

    assert expected_mean == pytest.approx(mean_error)
