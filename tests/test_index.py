import numpy as np
from unittest.mock import MagicMock

from nann import index

NBUCKETS = 2


def test_index_constructor():
    X = np.random.rand(10, 10)
    _get_test_index(X)


def test_index_split_returns_list_of_length_nbuckets():
    X = np.array([[0.1, 0.2], [0.2, 0.2], [0.5, 0.6], [0.3, 0.1]])
    idx = _get_test_index(X)

    split = idx.split()

    assert NBUCKETS == len(split)


def test_index_split_returns_partition():
    X = np.array([[0.1, 0.2], [0.2, 0.2], [0.5, 0.6], [0.3, 0.1]])
    idx = _get_test_index(X)

    split = idx.split()

    print(f"split: {split}")

    for part, _ in split:
        for x in part:
            assert x in X

    for x in X:
        exists = False
        for part, _ in split:
            for y in part:
                if (x == y).all():
                    exists = True
        assert exists


def _get_test_index(X):
    partition_idxs = np.array([0, 2, 4])
    inverse_partition = np.array([0, 3, 1, 2])
    index.Index._fit = MagicMock(return_value=(inverse_partition, partition_idxs, None))
    idx = index.Index(
        X=X,
        metric="some metric",
        S=-1,
        nbuckets=NBUCKETS,
        epochs=-1,
        kaffpa_path="whatever",
        kaffpa_preconfig="sure",
        model_type="regression",
        original_indices=np.arange(len(X)),
    )

    return idx
