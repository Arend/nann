import pytest
import numpy as np

from nann import bruteforce


@pytest.mark.parametrize(
    "metric, expected",
    [
        ("euclidean", np.array([[0, 1, 2], [1, 0, 2], [2, 1, 0]])),
        ("angular", np.array([[0, 1, 2], [1, 2, 0], [2, 1, 0]])),
    ],
)
def test_bruteforce_query_returns_nearest_neighbors(metric, expected):
    X = np.array([[-0.4, -0.4], [0.01, 0.0], [1.0, 0.9]])

    bf = bruteforce.BruteForce(metric)
    bf.fit(X)
    result = [bf.query(x, 3) for x in X]
    assert (expected == result).all()


@pytest.mark.parametrize(
    "metric, expected",
    [
        ("euclidean", np.array([[0, 1, 2], [1, 0, 2], [2, 1, 0]])),
        ("angular", np.array([[0, 1, 2], [1, 2, 0], [2, 1, 0]])),
    ],
)
def test_bruteforce_query_multiple_returns_nearest_neighbors(metric, expected):
    X = np.array([[-0.4, -0.4], [0.01, 0.0], [1.0, 0.9]])

    bf = bruteforce.BruteForce(metric)
    bf.fit(X)
    result = bf.query_multiple(X, 3)
    result_single = [bf.query(x, 3) for x in X]

    assert (expected == result).all()
