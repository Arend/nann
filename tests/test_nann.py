import numpy as np
import pytest
from unittest.mock import MagicMock

import nann.nann as nann


def test_get_top_p_buckets():
    distribution = np.array([[0.8, 0.0, 0.2]])
    p = 2
    top_buckets = np.array([0, 2])

    found_buckets = nann.get_top_p_buckets(distribution, p)

    assert (np.sort(top_buckets) == np.sort(found_buckets)).all()


@pytest.mark.parametrize(
    "f, expected_buckets", [(0.5, np.array([1, 3])), (1.0, np.array([1, 3, 2, 0, 4]))]
)
def test_get_top_buckets(f, expected_buckets):
    """f: minimum cumulative probability.
    """
    # we expect the buckets with probability 0.4 and 0.3 to be chosen
    # because they are the largest buckets with whose sum >= f
    distribution = np.array([0.1, 0.4, 0.2, 0.3, 0.1])

    buckets = nann.get_top_buckets(distribution, f)

    assert (expected_buckets == buckets).all()


def test_batch_get_top_buckets():
    # minimum cumulative probability
    f = 0.5
    distributions = np.array([[0.1, 0.2, 0.3], [0.3, 0.1, 0.2]])
    expected_buckets = [np.array([2, 1]), np.array([0, 2])]

    buckets = nann.batch_get_top_buckets(distributions, f)

    for i in range(len(expected_buckets)):
        assert (expected_buckets[i] == buckets[i]).all()


def test_get_subset():
    X = np.array([[0.1, 0.2], [0.2, 0.2], [0.5, 0.6], [0.3, 0.1]])
    # partition = np.array([0, 2, 2, 1])
    # so X[0] is in part 0, X[1] and X[2] in part 2 and X[3] in part 1
    # buckets == parts, so we want to pick part 0 and 2
    # i.e. X[0], X[1] and X[2]
    top_buckets = np.array([0, 2])
    # the below two lines mean this:
    # part 0 can be found in flat_inverse_partition[0:1]
    # part 1 can be found in flat_inverse_partition[1:2]
    # part 2 can be found in flat_inverse_partition[2:4]
    flat_inverse_partition = np.array([0, 3, 1, 2])
    flin_idxs = np.array([0, 1, 2, 4])
    original_indices = np.arange(len(X))

    expected_subset = np.array([[0.1, 0.2], [0.2, 0.2], [0.5, 0.6]])
    expected_indices = np.array([0, 1, 2])

    actual_subset, actual_indices = nann.get_subset(
        X, top_buckets, flat_inverse_partition, flin_idxs, original_indices
    )

    assert (expected_subset == actual_subset).all()
    assert (expected_indices == actual_indices).all()


def test_get_subset_on_subset_returns_original_indices():
    """Given an original dataset X,
    we take a subset Xn,
    partition Xn into 3,
    pick two non-adjacent parts,
    expect to see returned:
    the vertices from the 2 parts of Xn
    the indices of these vertices in X.
    """
    X = np.array([[0.1, 0.2], [0.8, 0.3], [0.2, 0.2], [0.5, 0.6], [0.3, 0.1]])
    Xn = np.array([[0.1, 0.2], [0.2, 0.2], [0.5, 0.6], [0.3, 0.1]])  # Note X[1] is gone
    X_indices = np.array([0, 2, 3, 4])
    top_buckets = np.array([0, 2])
    flat_inverse_partition = np.array([0, 3, 1, 2])
    flin_idxs = np.array([0, 1, 2, 4])

    expected_subset = np.array([[0.1, 0.2], [0.2, 0.2], [0.5, 0.6]])
    expected_original_indices = np.array([0, 2, 3])  # note these are in X

    actual_subset, actual_indices = nann.get_subset(
        Xn, top_buckets, flat_inverse_partition, flin_idxs, X_indices
    )

    assert (expected_subset == actual_subset).all()
    assert (expected_original_indices == actual_indices).all()


def test_get_subset_returns_original_indices():
    X = np.array([[0.1, 0.2], [0.2, 0.2], [0.3, 0.1]])
    # partition = np.array([0, 2, 1])
    top_buckets = np.array([2, 1])
    flin_idxs = np.array([0, 1, 2, 3])
    flat_inverse_partition = np.array([0, 2, 1])
    original_indices = np.arange(len(X))

    expected_subset = np.array([[0.2, 0.2], [0.3, 0.1]])
    expected_indices = np.array([1, 2])

    actual_subset, actual_indices = nann.get_subset(
        X, top_buckets, flat_inverse_partition, flin_idxs, original_indices
    )

    assert (expected_subset == actual_subset).all()
    assert (expected_indices == actual_indices).all()


def test_neighbors_returns_original_indices():
    query = np.array([0.3, 0.1])
    k = 1
    subset = np.array([[0.1, 0.2], [0.3, 0.1]])
    orig_indices = np.array([0, 2])
    metric = "euclidean"
    expected_neighbors = np.array([2])

    neighbors = nann.neighbors(query, k, subset, orig_indices, metric)

    assert (expected_neighbors == neighbors).all()
    assert expected_neighbors.shape == neighbors.shape
