#!/bin/bash

set -euxo pipefail

docker run -i --rm --mount type=bind,src=$1,target=/evaluation_datasets nann/evaluate /evaluation_datasets >> evaluation_results
