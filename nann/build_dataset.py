import os
import subprocess
import uuid
from typing import List, Tuple

import click
import h5py  # type: ignore
import numpy as np

from nann.superbrute import search_knn


def build_dataset(
    X: np.ndarray,
    k: int,
    n: int,
    m: int,
    metric: str,
    output_filepath: str,
    preconfig: str,
    test: np.ndarray = None,
    neighbors: np.ndarray = None,
    knn_graph_path: str = "knn.graph",
    partition_filepath: str = "kaffpa.partition",
) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """Create a dataset on which the graph partition predictor can be trained.
    First a kNN graph is built by brute force calculating the nearest neighbors
    of the n-sized subset X_train.
    Then the graph is partitioned using KaHIP.

    Parameters:
     - X: the full training (n, d) dataset;
     - k: the maximum number of nearest neighbors the dataset should "support";
     - n: the size of the training subset;
     - m: the number of parts in the partition;
     - metric: the distance metric used;
     - output_filepath;
     - test: for evaluation of the final prediction's accuracy;
     - neighbors: for the evaluation of the final prediction's accuracy;
     - partition_filepath: the (temporary) filename for the partitioning
       produced by kaffpa;
     - knn_graph_path: the path on which a pre built knn graph is saved.

    Returns:
     - X_train: the training subset of vectors from X, size n;
     - y_train: the partitioning as calculated by KaHIP;
     - X_test: the test subset;
     - y_test: the test partition subset;
     - X: the original (unshuffled) dataset.
    """
    X = get_subset(X, n)
    nns = calc_nearest_neighbors(X, k, metric)
    if not os.path.isfile(knn_graph_path):
        print("No knn graph found, creating new graph..")
        knn_graph = create_knn_graph(nns)
        knn_graph_path = "graph.knn"
        save_knn_graph(knn_graph, knn_graph_path)
    else:
        print(f"Loading knn graph from {knn_graph_path}")
    partition = create_partition(preconfig, knn_graph_path, m, partition_filepath)
    flat_inverted_partition, idxs = flatten_partition(partition)
    y = get_partition_distribution(partition, nns)
    X_train, y_train, X_test, y_test = split_dataset(X, y, 0.85)

    with h5py.File(output_filepath, "w") as outfile:
        outfile.create_dataset("X", data=X)
        outfile.create_dataset("y", data=y)
        outfile.create_dataset("X_train", data=X_train)
        outfile.create_dataset("y_train", data=y_train)
        outfile.create_dataset("X_test", data=X_test)
        outfile.create_dataset("y_test", data=y_test)
        outfile.create_dataset("flat_inverted_partition", data=flat_inverted_partition)
        outfile.create_dataset("fip_idxs", data=idxs)
        if test and neighbors:
            outfile.create_dataset("test", data=test)
            outfile.create_dataset("neighbors", data=neighbors)
        outfile.attrs["m"] = m
        outfile.attrs["metric"] = metric
    return X, y, flat_inverted_partition, idxs


def flatten_partition(partition):
    # m = number of partitions
    m = np.amax(partition) + 1
    # n = number of vectors
    n = partition.shape[0]
    # instantiate n lists
    inverted_partition = [[] for p in range(m)]
    # add vectors to their buckets
    for i, p in enumerate(partition):
        inverted_partition[p].append(i)
    flat_partition = []
    idxs = [None] * (m + 1)
    # add the "end index" to make parsing easier
    idxs[m] = n
    cur = 0
    for i, ip in enumerate(inverted_partition):
        # set starting index for this part
        idxs[i] = cur
        # update current index
        cur += len(ip)
        flat_partition += ip
    return np.array(flat_partition, dtype="i"), np.array(idxs, dtype="i")


def save_inverse_partition(inverse_partition, partition_idxs, filename):
    with h5py.File(filename, "w") as outfile:
        outfile.create_dataset("inverse_partition", data=inverse_partition)
        outfile.create_dataset("partition_idxs", data=partition_idxs)
    return filename


def get_subset(X, n):
    """Take a subset of X for brute force calculation.

    Parameters:
     - X
     - n: the size of the subset. If n==0, take all points in X.

    Returns:
     - X_train: a np array
    """
    if n == 0:
        return np.array(X)
    else:
        return np.array(X[:n])


def calc_nearest_neighbors(X: np.ndarray, k: int, metric: str) -> np.ndarray:
    # k can't be larger than the number of vectors
    assert not k > X.shape[0]
    X = X.astype("float32")
    D, I = search_knn(X, X, k, metric)
    return I


def save_nns(nns: np.ndarray, filename: str) -> str:
    with h5py.File(filename, "w") as outfile:
        outfile.create_dataset("nns", data=nns)
    return filename


def create_knn_graph(nns: np.ndarray) -> str:
    """Brute force calculate nearest neighbors and create an adjacency list
    that can be fed to KaHIP kaffpa for partitioning.

    Parameters:
     - nns: the full nearest neighbors dataset, brute forced.

    Returns:
     - knn_graph, symmetric, as a string.
    """
    # bf.query returns vector as its own nearest neighbor
    # but kaffpa does not allow self edges
    # so remove first neighbor
    nns = nns[:, 1:]
    n = len(nns)

    # make symmetric
    nns, m = symmetric(nns)

    # convert to strings and start index at 1
    nns = [[str(y + 1) for y in x] for x in nns]  # type: ignore
    # join inner lists and outer list
    knn_graph = "\n".join([" ".join(x) for x in nns])
    knn_graph = f"{n} {m}\n" + knn_graph
    return knn_graph


def save_knn_graph(knn_graph: str, filename: str) -> str:
    with open(filename, "w") as outfile:
        outfile.write(knn_graph)
    return filename


def symmetric(nns: np.ndarray) -> Tuple[np.ndarray, int]:
    """Make a nearest neighbors list symmetric, e.g. if node 1 has an edge to
    node 2, make sure node 2 also has an edge to 1.

    Parameters:
     - nns: a 2d numpy array with nearest neighbors, where nodes are indexed
            starting from 0, so row 0 contains the nearest neighbors of node 0
    Returns:
     - symmetric: a list of lists with nearest neighbors, only now symmetric
     - m:   the edge count divided by two (since edges are counted as if they
            were undirected)
    """
    symmetric = [list(nn) for nn in nns]
    m = nns.size
    for vector, neighbors in enumerate(nns):
        for neighbor in neighbors:
            if vector not in nns[neighbor]:
                assert vector != neighbor
                symmetric[neighbor].append(vector)
                m += 1
    # although edges are represented twice, they are only counted once
    m = m // 2
    return np.array(symmetric), m


def create_partition(preconfig: str, knn_graph_path: str, m: int) -> np.ndarray:
    """Save knn_graph as a text file so it can be consumed by KaHIP kaffpa.
    Call kaffpa as a subprocess, which will store the result in an output file.

    Parameters:
     - preconfiguration: the kaffpa preconfiguration to use (fast, eco or
     strong);
     - knn_graph_path: the path where the knn graph can be found;
     - m: the number of parts (=buckets) to partition into;
     - partition_path: the path where kaffpa should save the partition.

    Returns:
     - the partition as a numpy array.
    """
    partition_path = str(uuid.uuid4()) + ".partition"
    # build & run command
    cmd = [
        os.environ["KAFFPA_PATH"],
        f"--preconfiguration={preconfig}",
        f"--k={m}",
        f"--output_filename={partition_path}",
        knn_graph_path,
    ]
    print(cmd)
    subprocess.run(cmd, check=True)

    # load the partition as a numpy array
    with open(partition_path, "r") as partition_infile:
        partition = np.array(partition_infile.readlines(), dtype="i")
    os.remove(partition_path)
    return partition


def get_partition_distribution(partition, nns):
    minlength = np.amax(partition) + 1
    return np.array([np.bincount(part, minlength=minlength) for part in partition[nns]])


def save_partition_distribution(y, filename):
    with h5py.File(filename, "w") as outfile:
        outfile.create_dataset("y", data=y)
    return filename


def split_dataset(X, y, train_portion):
    """Split up the dataset (X, y) into train and test, in order to evaluate
    the performance of the model.

    Parameters:
     - X, y: the dataset (numpy arrays).
     - train_size: the part of the dataset that should be used for
       training the model (e.g. 0.7 means that 70% of the dataset is used).

    Returns:
     - X_train, y_train, X_test, y_test
    """
    assert len(X) == len(y)

    # shuffle both arrays simultatuously
    p = np.random.permutation(len(y))
    X, y = (X[p], y[p])

    train_size = int(len(y) * train_portion)

    return (X[:train_size], y[:train_size], X[train_size:], y[train_size:])


@click.command()
@click.argument("source-dataset", type=click.Path(exists=True))
@click.argument("kaffpa-path", type=click.Path(exists=True))
@click.option(
    "--output-filepath", default="dataset.h5", show_default=True, type=click.Path()
)
@click.option(
    "-k",
    help="The number of neighbors to calculate.",
    default=100,
    show_default=True,
    type=int,
)
@click.option(
    "-n",
    help="The number of points to use for dataset creation (default is all points).",
    default=0,
    type=int,
)
@click.option(
    "-m",
    help="The number of parts kaffpa should partition the kNN graph in.",
    default=16,
    show_default=True,
    type=int,
)
@click.option(
    "--knn-graph",
    help="A pre-built knn graph",
    type=click.Path(),
    default="knn.graph",
    show_default=True,
)
@click.option(
    "--preconfiguration",
    help="The kaffpa preconfiguration setting to use",
    type=str,
    default="fast",
    show_default=True,
)
def main(
    source_dataset, kaffpa_path, output_filepath, k, n, m, knn_graph, preconfiguration
):
    ds_in = h5py.File(source_dataset, "r")
    X = ds_in["train"]
    metric = ds_in.attrs["distance"]
    test = ds_in["test"]
    neighbors = ds_in["neighbors"]
    os.environ["KAFFPA_PATH"] = kaffpa_path
    build_dataset(
        X,
        k,
        n,
        m,
        metric,
        output_filepath,
        preconfiguration,
        test,
        neighbors,
        knn_graph,
    )
