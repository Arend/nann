from abc import ABC, abstractmethod

import numpy as np


class PartPredictor(ABC):
    def __init__(self, nbuckets, **kwargs):
        self.nbuckets = nbuckets

    @abstractmethod
    def fit(self, X_train, y_train, epochs, verbose=1):
        pass

    @abstractmethod
    def predict_one(self, x):
        pass

    @abstractmethod
    def predict_multiple(self, X):
        pass

    @abstractmethod
    def save(self, filename):
        pass

    @abstractmethod
    def load(self, filename):
        pass
