# author: Matthijs Douze
# source: https://gist.github.com/mdouze/8e47d8a5f28280df7de7841f8d77048d

import faiss
import h5py
import numpy as np
import sklearn.preprocessing


def search_knn(xq, xb, k, distance_type):
    """ wrapper around the faiss knn functions without index """
    if distance_type not in ["euclidean", "angular"]:
        raise Exception(f"Metric not supported: {distance_type}")
    if distance_type == "angular":
        xb = sklearn.preprocessing.normalize(xb, axis=1, norm="l2")
    nq, d = xq.shape
    nb, d2 = xb.shape
    assert d == d2

    I = np.empty((nq, k), dtype="int64")
    D = np.empty((nq, k), dtype="float32")

    heaps = faiss.float_maxheap_array_t()
    heaps.k = k
    heaps.nh = nq
    heaps.val = faiss.swig_ptr(D)
    heaps.ids = faiss.swig_ptr(I)
    faiss.knn_L2sqr(faiss.swig_ptr(xq), faiss.swig_ptr(xb), d, nq, nb, heaps)

    return D, I
