import keras
import numpy as np
import tensorflow as tf

from nann.part_predictor import PartPredictor


class NeuralLSH(PartPredictor):
    def __init__(self, nbuckets, **kwargs):
        PartPredictor.__init__(self, nbuckets)
        self.layer_size = kwargs.get("layer_size", 512)
        self.nblocks = kwargs.get("nblocks", 3)

    def _compile(self):
        blocks = []
        for b in range(self.nblocks):
            blocks.append(keras.layers.Dense(self.layer_size, activation=tf.nn.relu))
            blocks.append(keras.layers.BatchNormalization(axis=1))
        blocks.append(keras.layers.Dense(self.nbuckets, activation=tf.nn.softmax))
        self.model = keras.Sequential(blocks)
        self.model.compile(
            optimizer="adam", loss="kullback_leibler_divergence", metrics=["accuracy"]
        )

    def fit(self, X_train, y_train, epochs, verbose=1):
        self._compile()
        self.model.fit(X_train, y_train, epochs=epochs, verbose=verbose)

    def predict_one(self, x):
        x = np.array([x])
        return self.predict_multiple(x)[0]

    def predict_multiple(self, X):
        return self.model.predict(X)

    def save(self, filename):
        self.model.save(filename)

    def load(self, filename):
        self.model = keras.models.load_model(filename)
