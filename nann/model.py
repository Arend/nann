import click
import h5py
import numpy as np

from nann.kmeans import KMeans
from nann.neural_lsh import NeuralLSH
from nann.regression_lsh import RegressionLSH

MODELS = {"neural": NeuralLSH, "kmeans": KMeans, "regression": RegressionLSH}


def get_model(model_type: str, nbuckets: int, **kwargs) -> any:
    if model_type not in MODELS:
        raise NotImplementedError(
            f"No implementation found for model type {model_type}"
        )
    model = MODELS[model_type](nbuckets, **kwargs)  # type: ignore
    return model


def get_model_names() -> None:
    for k in MODELS:
        yield k


@click.command()
@click.option(
    "--dataset",
    required=True,
    type=click.Path(exists=True),
    help="Dataset as produced by build_dataset.",
)
@click.option(
    "--epochs",
    default=20,
    show_default=True,
    help="Number of epochs the model should train.",
)
@click.option(
    "--filename",
    default="model.h5",
    show_default=True,
    help="The filename the trained model should be saved as",
)
@click.option(
    "--model-type",
    type=click.Choice(["kmeans", "neural", "hierarchical"]),
    default="neural",
    show_default=True,
    help="The type of model to use.",
)
def main(dataset, epochs, filename, model_type):
    """Fit a model to a give dataset and save the results.
    """

    with h5py.File(dataset, "r") as ds:
        X_train = np.array(ds["X_train"])
        y_train = np.array(ds["y_train"])
        m = ds.attrs["m"]

    model = get_model(model_type, m)
    model.fit(X_train, y_train, epochs)
    model.save(filename)
