import numpy as np
from joblib import dump, load
from sklearn.linear_model import LogisticRegression

from nann.part_predictor import PartPredictor


class RegressionLSH(PartPredictor):
    def __init__(self, nbuckets, **kwargs):
        kwargs.setdefault("max_iter", 10000)
        max_iter = kwargs.get("max_iter")
        self.lr = LogisticRegression(
            multi_class="multinomial", solver="saga", max_iter=max_iter
        )
        PartPredictor.__init__(self, nbuckets)

    def fit(self, X, y, epochs, verbose=0):
        # convert label into binary value: bucket 0 if most of the
        # distribution is in bucket 0, and vice versa
        y = y.argmax(axis=1)

        try:
            assert len(np.unique(y)) == self.nbuckets
        except:
            raise ValueError(
                "The training data does not contain examples "
                + "of the highest count being in every bucket. "
                + "This would lead RegressionLSH to predict "
                + "for an incorrect number of buckets."
            )
        self.lr.fit(X, y)

    def predict_one(self, x):
        x = np.array([x])
        return self.predict_multiple(x)[0]

    def predict_multiple(self, X):
        return self.lr.predict_proba(X)

    def save(self, filename):
        dump(self.lr, filename)

    def load(self, filename):
        self.lr = load(filename)
