import os
import sys
import time
from typing import List, Tuple

import click
import h5py  # type: ignore
import numpy as np  # type: ignore
from joblib import Memory, Parallel, delayed
from scipy import stats  # type: ignore

from nann.model import get_model
from nann.index import fit_model

memory = Memory(".evaluate_cache", verbose=0)


def evaluate(
    model_type: str, dataset_filename: str, epochs: int, **kwargs
) -> Tuple[float, float, int]:
    metric = _get_metric(dataset_filename)
    X_test, y_test, X_train, y_train = _get_ds(dataset_filename)
    nbuckets = y_test.shape[1]
    model = get_model(model_type, nbuckets, **kwargs)  # type: ignore
    old_stdout = sys.stdout
    sys.stdout = sys.stderr
    model = memory.cache(fit_model)(model, X_train, y_train, epochs, verbose=1)
    sys.stdout = old_stdout
    t0 = time.time()
    # predictions only for timing, always predict same number of vectors
    predictions = model.predict_multiple(X_test[:1000])
    t1 = time.time()
    queries_per_second = int(len(predictions) / (t1 - t0))

    predictions = model.predict_multiple(X_test)
    entropy = stats.entropy(np.transpose(y_test), np.transpose(predictions))
    # normalize entropy (0, 1)
    error = 1 - np.exp(-entropy)
    stdev = np.std(error)
    error = np.mean(error)
    return error, stdev, queries_per_second


def _get_ds(
    dataset_filename: str
) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    with h5py.File(dataset_filename, "r") as ds:
        y_test = np.array(ds["y_test"])
        X_test = np.array(ds["X_test"])
        y_train = np.array(ds["y_train"])
        X_train = np.array(ds["X_train"])
    return X_test, y_test, X_train, y_train


def _get_file_names(filepath: str) -> List[str]:
    if os.path.isfile(filepath):
        return [filepath]
    assert os.path.isdir(filepath)
    paths = list()
    for fp in os.listdir(filepath):
        fp = os.path.join(filepath, fp)
        if os.path.isfile(fp):
            paths.append(fp)
    return paths


def _get_metric(dataset_filename: str) -> str:
    dataset_filename = dataset_filename.lower()
    if "euclidean" in dataset_filename:
        return "euclidean"
    if "angular" in dataset_filename:
        return "angular"
    else:
        raise NotImplementedError(
            f"No supported metric found in filename: {dataset_filename}"
        )


def _do_loop(
    dsname: str,
    ds: str,
    epochss: List[int] = [1, 2, 3, 5, 10, 50],
    layer_sizes: List[int] = [1, 8, 64, 128, 512],
    nblockss: List[int] = [1, 2, 3, 4],
):
    error, stdev, queries_per_second = evaluate("kmeans", ds, 1)
    click.echo(f"{dsname},kmeans,-1,-1,-1,{error},{stdev},{queries_per_second}")

    for epochs in epochss:
        for layer_size in layer_sizes:
            for nblocks in nblockss:
                error, stdev, queries_per_second = evaluate(
                    "neural", ds, epochs, layer_size=layer_size, nblocks=nblocks
                )
                click.echo(
                    f"{dsname},neural,{epochs},{layer_size},{nblocks},{error},{stdev},{queries_per_second}"
                )


@click.command()
@click.argument("dataset_folder")
@click.option("-n", "--n-jobs", default=1, help="number of datasets to run in parallel")
def cli(dataset_folder: str, n_jobs: int) -> None:
    """Evaluate models and datasets.
    """
    datasets = _get_file_names(dataset_folder)
    click.echo(
        "dataset,model_type,epochs,layer_size,nblocks,error,stdev,queries_per_second"
    )
    dsnames = [os.path.splitext(os.path.basename(ds))[0] for ds in datasets]
    Parallel(n_jobs=n_jobs)(
        delayed(_do_loop)(dsname, ds) for dsname, ds in zip(dsnames, datasets)
    )


if __name__ == "__main__":
    cli()
