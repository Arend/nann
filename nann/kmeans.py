import numpy as np
from joblib import dump, load
from sklearn.cluster import KMeans as sKMeans

from nann.part_predictor import PartPredictor


class KMeans(PartPredictor):
    def __init__(self, nbuckets, **kwargs):
        PartPredictor.__init__(self, nbuckets)
        self.km = sKMeans(nbuckets, precompute_distances=True)

    def fit(self, X, y, epochs, verbose=None):
        self.km.fit(X)

    def predict_one(self, x):
        x = np.array([x])
        return self.predict_multiple(x)[0]

    def predict_multiple(self, X):
        # calculate distance to each cluster
        dist = self.km.transform(X)
        # normalize
        dist = self._normalize(dist)
        # invert
        inverted = 1 - dist
        # normalize again
        return self._normalize(inverted)

    def save(self, filename):
        dump(self.km, filename)

    def load(self, filename):
        self.km = load(filename)

    def _normalize(self, dist):
        dsum = np.sum(dist, axis=1, keepdims=True)
        return dist / dsum
