import numpy as np

from nann.index import Index
from nann import nann


class MultiIndex:
    def __init__(
        self,
        X,
        metric,
        S,
        nbuckets,
        epochs,
        kaffpa_path,
        kaffpa_preconfig,
        nlayers,
        model_type,
        original_indices,
        **kwargs,
    ):
        self.X = X
        self.metric = metric
        self.S = S
        self.nbuckets = nbuckets
        self.epochs = epochs
        self.kaffpa_path = kaffpa_path
        self.kaffpa_preconfig = kaffpa_preconfig
        self.nlayers = nlayers
        self.model_type = model_type
        self.original_indices = original_indices
        self.kwargs = kwargs
        self.dist_comps = 0
        self._fit(X)

    def reset_dist_comps():
        self.dist_comps = 0

    def add_layer(self):
        """Add nbuckets child MultiIndex objects.
        """
        X_split = self.top_index.split()
        model_type = self.model_type
        if model_type == "neural" and self.nbuckets > 255:
            # if nbuckets is 256 (or more), make secondary layer(s) kmeans instead of neural
            model_type = "kmeans"
        self.lower_idxs = [
            MultiIndex(
                Xn,
                self.metric,
                self.S,
                self.nbuckets,
                self.epochs,
                self.kaffpa_path,
                self.kaffpa_preconfig,
                self.nlayers - 1,
                model_type,
                original_indices,
                **self.kwargs,
            )
            for Xn, original_indices in X_split
        ]

    def _fit(self, X):
        self.top_index = Index(
            X,
            self.metric,
            self.S,
            self.nbuckets,
            self.epochs,
            self.kaffpa_path,
            self.kaffpa_preconfig,
            self.model_type,
            self.original_indices,
            **self.kwargs,
        )
        if self.nlayers > 1:
            self.add_layer()

    def get_subset(self, x, f):
        if self.nlayers == 1:
            subset, mapped_indices = self.top_index.get_subset(x, f)
            return subset, mapped_indices
        else:
            # pick lower Index objects to use
            lower_indexes_to_check = self.top_index.get_top_buckets(x, f)

            # query lower Index objects and append results
            subset = None
            mapped_indices = None
            for lidx in lower_indexes_to_check:
                lidx = self.lower_idxs[lidx]
                s, m = lidx.get_subset(x, f)
                if subset is None:
                    subset = s
                    mapped_indices = m
                else:
                    subset = np.concatenate((subset, s))
                    mapped_indices = np.concatenate((mapped_indices, m))

            return subset, mapped_indices

    def query(self, x, k, f):
        subset, mapped_indices = self.get_subset(x, f)
        self.dist_comps += len(subset)
        return nann.neighbors(x, k, subset, mapped_indices, self.metric)

    def batch_query(self, X, k, f):
        raise NotImplementedError
