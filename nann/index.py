import hashlib
import os
import shutil
import time
from typing import List, Tuple

import h5py  # type: ignore
import numpy as np  # type: ignore
from joblib import Memory

import nann.build_dataset as dataset
import nann.nann as nann
from nann.model import get_model
from nann.part_predictor import PartPredictor

# set cache folder location
CACHE_FOLDER = ".nann_cache"
memory = Memory(CACHE_FOLDER)


def fit_model(
    model: PartPredictor, X: np.ndarray, y: np.ndarray, epochs: int, verbose=1
) -> PartPredictor:
    model.fit(X, y, epochs, verbose=verbose)
    return model


class Index:
    def __init__(
        self,
        X: np.ndarray,
        metric: str,
        S: int,
        nbuckets: int,
        epochs: int,
        kaffpa_path: str,
        kaffpa_preconfig: str,
        model_type: str,
        original_indices: np.ndarray,
        **kwargs,
    ) -> None:
        self.X = X
        self.metric = metric
        self.S = S
        self.nbuckets = nbuckets
        self.epochs = epochs
        os.environ["KAFFPA_PATH"] = kaffpa_path
        self.kaffpa_preconfig = kaffpa_preconfig
        self.model_type = model_type
        self.model = get_model(model_type, nbuckets, **kwargs)  # type: ignore
        self.inverse_partition, self.partition_idxs, self.model = self._fit(
            X, S, metric, kaffpa_preconfig, nbuckets, self.model, epochs
        )
        self.original_indices = original_indices
        self.dist_comps = 0

    def reset_dist_comps():
        self.dist_comps = 0

    def split(self) -> List[Tuple[np.ndarray, np.ndarray]]:
        """Split the original dataset into m parts based on the partition
        made for this index.
        Returns:
        - a list of length self.nbuckets containing a partition of self.X
        """
        split_X = []
        for i in range(self.nbuckets):
            subset, original_indices = nann.get_subset(
                self.X,
                np.array([i]),
                self.inverse_partition,
                self.partition_idxs,
                self.original_indices,
            )
            split_X.append((subset, original_indices))
        return split_X

    def _fit(
        self,
        X: np.ndarray,
        S: int,
        metric: str,
        kaffpa_preconfig: str,
        nbuckets: int,
        model: PartPredictor,
        epochs: int,
    ) -> Tuple[np.ndarray, np.ndarray, PartPredictor]:
        """Fit the model to a given dataset of vectors X of shape (n, d), where
        n = the number of vectors and d = each vector's dimension.
        Parameters:
         - X: the dataset;
         - S: the number of nearest neighbors to base the training on;
         - metric: the distance metric (e.g. euclidean, angular);
         - original_indices: if a dataset is split up across multiple Index
             instances, pass along the original indices of the vectors in the
             subsets, so that the sub-Index can return the correct index (i.e.
             the correct position of the vector in the original dataset).
        """
        print("Calculating full nearest neighbors set..")
        nns = memory.cache(dataset.calc_nearest_neighbors)(X, S, metric)
        # make a name unique to this dataset
        # NOTE this shouldn't be done in parallel!
        dataset_hash = hashlib.sha1(X[:20, :1].flatten()).hexdigest()
        knn_graphname = dataset_hash + str(S) + metric + ".knn"
        knn_graphname = os.path.join(CACHE_FOLDER, knn_graphname)
        if not os.path.isfile(knn_graphname):
            print("Building KNN graph..")
            knn_graph = memory.cache(dataset.create_knn_graph)(nns)
            print(f"Writing KNN graph to {knn_graphname}")
            dataset.save_knn_graph(knn_graph, knn_graphname)
        print("Partitioning..")
        partition = memory.cache(dataset.create_partition)(
            kaffpa_preconfig, knn_graphname, nbuckets
        )
        print("Inverting partition..")
        inverse_partition, partition_idxs = memory.cache(dataset.flatten_partition)(
            partition
        )
        print("Preparing training labels..")
        y = memory.cache(dataset.get_partition_distribution)(partition, nns)
        print("Training model..")
        model = memory.cache(fit_model)(model, X, y, epochs)
        return inverse_partition, partition_idxs, model

    def get_top_buckets(self, x: np.ndarray, f: float) -> np.ndarray:
        distribution = self.model.predict_one(x)
        return nann.get_top_buckets(distribution, f)

    def get_subset(self, x: np.ndarray, f: float) -> Tuple[np.ndarray, np.ndarray]:
        top_buckets = self.get_top_buckets(x, f)

        # get the candidate set from X based on the selected buckets
        subset, mapped_indices = nann.get_subset(
            self.X,
            top_buckets,
            self.inverse_partition,
            self.partition_idxs,
            self.original_indices,
        )
        return subset, mapped_indices

    def query(self, x: np.ndarray, k: int, f: float) -> np.ndarray:
        subset, mapped_indices = self.get_subset(x, f)

        self.dist_comps += len(subset)

        # brute force calculate the nearest neighbors from the candidate set
        neighbors = nann.neighbors(x, k, subset, mapped_indices, self.metric)
        return neighbors

    def batch_query(self, X: np.ndarray, k: int, f: float) -> List[np.ndarray]:
        # get the predicted partition distributions:
        distributions = self.model.predict_multiple(X)

        top_buckets = nann.batch_get_top_buckets(distributions, f)

        # get the candidate set from X based on the selected buckets
        subsets = [
            nann.get_subset(
                self.X,
                top_bucket,
                self.inverse_partition,
                self.partition_idxs,
                self.original_indices,
            )
            for top_bucket in top_buckets
        ]

        for subset in subsets:
            self.dist_comps += len(subset[0])

        results = [
            nann.neighbors(x, k, subset[0], subset[1], self.metric)
            for x, subset in zip(X, subsets)
        ]
        return results
