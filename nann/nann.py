from typing import Tuple

import click
import h5py
import keras.models
import numpy as np

from nann.superbrute import search_knn


def get_top_p_buckets(distributions: np.ndarray, p: int) -> np.ndarray:
    """Get the p buckets that are most likely to contain the nearest neighbors
    (according to the distribution).
    Parameters:
     - distributions: n distributions predicted by the model shape (n,nbuckets);
     - p: the number of buckets to choose.
    Returns:
     - the indices of the p top buckets (unsorted) for each distribution.
    """
    return np.argpartition(distributions, -p)[:, -p:]


def get_top_buckets(distribution: np.ndarray, f: float) -> np.ndarray:
    """Get the buckets most likely to contain the nearest neighbors,
    such that the cumulative probability is at least f.
    Parameters:
     - distribution: the distribution predicted by the model;
     - f: the minimum cumulative probability, where f=1.0 means 'pick all
          buckets'.
    Returns:
     - the indices of the top buckets (sorted)
    """
    # indices sorted by value descending
    sorted_idxs = np.argsort(-distribution)
    # distribution sorted by value (probability) descending
    sorted_dist = distribution[sorted_idxs]
    # cumulative sum of distribution
    cumsum = np.cumsum(sorted_dist)
    # the first position in sorted_idxs where the cumulative sum is >= f
    slice_idx = np.nonzero(cumsum >= f)[0][0]
    return sorted_idxs[: slice_idx + 1]


def batch_get_top_buckets(distributions: np.ndarray, f: float):
    """Batch version of get_top_buckets.
    Parameters:
     - distributions: the array of n distributions predicted by the model;
     - f: the minimum cumulative probability, where f=1.0 means 'pick all
          buckets'.
    Returns:
     - the indices of the top buckets (sorted) for n distributions.
    """
    # indices sorted by value descending
    sorted_idxs = np.argsort(-distributions)
    # distributions sorted by value (probability) descending
    sorted_dist = -np.sort(-distributions)
    # cumulative sums of distributions
    cumsum = np.cumsum(sorted_dist, axis=1)
    # indices in both dimensions of values >= f
    larger_equal_f = np.nonzero(cumsum >= f)
    # column indices of first values >= f in each row
    ci = np.unique(larger_equal_f[0], return_index=True)[1]
    # corresponding row indices
    ri = larger_equal_f[1][ci]
    top_buckets = [sorted_idxs[i][: ri[i] + 1] for i in range(len(ri))]
    return top_buckets


def get_subset(
    X: np.ndarray,
    top_buckets: np.ndarray,
    flat_inverse_partition: np.ndarray,
    fl_in_idxs: np.ndarray,
    original_indices: np.ndarray,
) -> Tuple[np.ndarray, np.ndarray]:
    """Get the points from X that are in top_buckets.
    Parameters:
     - X: the vector array (n, dim);
     - top_buckets: the buckets containing the points we want from X;
     - flat_inverse_partition: the flattened inverse partition of X (n,);
     - fl_in_idxs: the indices for retrieving parts from the flattened
                   inverse partition (m+1,);
     - original_idxs: the original indices of the points in X (if X is the
                      entire dataset of n vectors, this is [1, ..., n-1].
    Returns:
     - subset: the array of vectors in the subset shape (subset_size, dim);
     - orig_indices: the original indices in X of the subset.
    """
    subset_indices = None
    for ind in top_buckets:
        start = fl_in_idxs[ind]
        stop = fl_in_idxs[ind + 1]
        part = flat_inverse_partition[start:stop]
        if subset_indices is None:
            subset_indices = part
        else:
            subset_indices = np.concatenate((subset_indices, part))
    return X[subset_indices], original_indices[subset_indices]


def neighbors(
    query: np.ndarray, k: int, subset: np.ndarray, orig_indices: np.ndarray, metric: str
) -> np.ndarray:
    """Brute force calculate the nearest neighbors in the (candidate) subset.
    Parameters:
     - query: the query vector;
     - k: the number of nearest neighbors to find;
     - subset: the (candidate) subset of vectors to scan;
     - orig_indices: the indices of the subset in X;
     - metric: the distance metric used.
    Returns:
     - nbrs: the indices of the k nearest neighbors.
    """
    query = np.array([query]).astype("float32")
    subset = subset.astype("float32")
    _, nbrs = search_knn(query, subset, k, metric)
    nbrs = orig_indices[nbrs]
    return nbrs[0]


@click.command()
@click.option(
    "--model",
    required=True,
    type=click.Path(exists=True),
    help="The model trained to predict the placement of a query vector in a partitioned kNN graph.",
)
@click.option(
    "--dataset",
    required=True,
    type=click.Path(exists=True),
    help="The dataset produced by build_dataset.",
)
@click.option(
    "--query",
    required=False,
    type=click.Path(exists=True),
    help="noop The query vector you would like to predict nearest neighbors for.",
)
@click.option(
    "-k",
    default=100,
    show_default=True,
    help="The number of neighbors to calculate for the query",
)
@click.option(
    "-p",
    default=3,
    show_default=True,
    help="The number of parts from the partition to brute force query in the final step.",
)
@click.option("--debug/--no-debug", default=False)
def main(model, dataset, query, k, p, debug):  # pragma: no cover
    """Neural Approximate Nearest Neighbors (NANN).
    """
    with h5py.File(dataset, "r") as ds:
        X = np.array(ds["X"])
        y = np.array(ds["y"])
        flat_partition = np.array(ds["flat_inverted_partition"])
        idxs = np.array(ds["fip_idxs"])
        test = np.array(ds["test"])
        test_neighbors = np.array(ds["neighbors"])
        metric = str(ds.attrs["metric"])
        m = int(ds.attrs["m"])

    model = keras.models.load_model(model)

    correct = 0
    incorrect = 0
    test_count = test.shape[0]
    prediction_time = 0.0
    bruteforce_time = 0.0

    full_match = 0
    total = test.shape[0]
    for test_idx in range(total):
        # compare with neighbors in generated dataset
        query = test[test_idx]
        answer = test_neighbors[test_idx]
        import time

        t0 = time.time()
        top_buckets = predict_top_buckets(query, p, model)
        subset, orig_indices = get_subset(X, top_buckets, flat_partition, idxs)
        t1 = time.time()
        nbrs = neighbors(query, k, subset, orig_indices, metric)
        t2 = time.time()

        prediction_time += t1 - t0
        bruteforce_time += t2 - t1
        click.echo(f"Calculated neighbors: {nbrs}")

        print(f"Expected neighbors: {answer}")
        for nb in nbrs:
            if nb in answer:
                correct += 1
            else:
                incorrect += 1
        if (answer == nbrs).all():
            full_match += 1
    print(f"Correct: {correct}")
    print(f"Incorrect: {incorrect}")
    print(f"Expected neighbors is 100% identical (fishy): {(full_match/total)*100}%")

    click.echo(f"avg subset prediction (ms): {1000*prediction_time/test_count}")
    click.echo(f"avg bruteforce (ms): {1000*bruteforce_time/test_count}")
    if not debug:
        return
    # Let's do a little test.
    # first, take a known neighbor:
    nb1 = X[answer[1]]
    # then, take a predicted neighbor:
    nb2 = X[nbrs[1]]
    # now, let's measure the distances in relation to the query vector
    vector_array = np.array([query, nb1, nb2])
    import sklearn.metrics

    distances = sklearn.metrics.pairwise_distances(vector_array)
    print(distances)
