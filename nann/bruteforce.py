"""
Copied from ann-benchmarks:
https://github.com/erikbern/ann-benchmarks/blob/e192e7c8155546ab549773cdaa2de943b0614038/ann_benchmarks/algorithms/bruteforce.py
"""
import sklearn.neighbors


class BruteForce:
    def __init__(self, metric):
        if metric not in ("angular", "euclidean", "hamming"):
            raise NotImplementedError("BruteForce doesn't support metric %s" % metric)
        self._metric = metric
        self.name = "BruteForce()"

    def fit(self, X):
        metric = {"angular": "cosine", "euclidean": "l2", "hamming": "hamming"}[
            self._metric
        ]
        self._nbrs = sklearn.neighbors.NearestNeighbors(
            algorithm="brute", metric=metric
        )
        self._nbrs.fit(X)

    def query_multiple(self, vv, n):
        return self._nbrs.kneighbors(vv, return_distance=False, n_neighbors=n)

    def query(self, v, n):
        v = [v]
        nbrs = self._nbrs.kneighbors(v, return_distance=False, n_neighbors=n)
        return nbrs[0]

    def query_with_distances(self, v, n):
        (distances, positions) = self._nbrs.kneighbors(
            [v], return_distance=True, n_neighbors=n
        )
        return zip(list(positions[0]), list(distances[0]))
