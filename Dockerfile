FROM python:3.6-stretch AS base
RUN apt-get update && apt-get install -y gcc make libopenblas-base libomp-dev
COPY . /opt
COPY --from=arendvosmaer/kahip /KaHIP/ /KaHIP/
WORKDIR /opt

FROM base as test
ENV PYTHONDONTWRITEBYTECODE=true
RUN pip install -r requirements.txt -r test_requirements.txt
RUN pip install .

FROM test as spellcheck
ENV ASPELL_SERVER ftp://ftp.gnu.org/gnu/aspell
ENV ASPELL_VERSION 0.60.6.1
ENV ASPELL_EN 2015.04.24-0
RUN curl -SLO "${ASPELL_SERVER}/aspell-${ASPELL_VERSION}.tar.gz" \
  && curl -SLO "${ASPELL_SERVER}/dict/en/aspell6-en-${ASPELL_EN}.tar.bz2" \
  && tar -xzf "/opt/aspell-${ASPELL_VERSION}.tar.gz" \
  && tar -xjf "/opt/aspell6-en-${ASPELL_EN}.tar.bz2" \
  && cd "/opt/aspell-${ASPELL_VERSION}" \
    && ./configure \
    && make \
    && make install \
    && ldconfig \
  && cd "/opt/aspell6-en-${ASPELL_EN}" \
    && ./configure \
    && make \
    && make install \
  && rm -rf /aspell* /var/lib/apt/lists/*
RUN make run-all-tests
