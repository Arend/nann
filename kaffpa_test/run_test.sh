#!/bin/bash

set -euxo pipefail

preconfigs=('fast' 'eco' 'strong')
datasets=('glove-100-angular.hdf5' 'mnist-784-euclidean.hdf5')

for ds in "${datasets[@]}"; do
    for preconfig in "${preconfigs[@]}"; do
		for m in {16,256}; do
			for k in {25,50,75,100}; do
	    		fname=${ds%.hdf5}
				fname="results/20190704/"$fname"_"$preconfig"_"$m"_"$k
				dataset --output-filepath $fname".dataset" -k $k -n 10000 -m $m --knn-graph $fname".knn" --preconfiguration $preconfig "../datasets/"$ds ../../KaHIP/deploy/kaffpa
			done
	    done
	done
done
