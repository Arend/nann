import glob
import os
import sys

import h5py
import numpy as np

path = sys.argv[1]

# get all ds filenames
datasets = glob.glob(os.path.join(path, "*.dataset"))
print("dataset,preconfig,m,k,count,count/m/k")
# for each ds:
for ds in datasets:
    # parse filename: dataset, preconfig, m and k
    ds_basename = os.path.basename(ds)
    ds_name, preconfig, m, k = ds_basename.split(".")[0].split("_")
    # load y
    with h5py.File(ds, "r") as dsin:
        y = np.array(dsin["y"])
    for dist in y:
        # count number of used buckets
        count = np.count_nonzero(dist)
        # print ds_name, preconfig, m, k, count
        print(f"{ds_name},{preconfig},{m},{k},{count},{count/int(m)/int(k)}")
