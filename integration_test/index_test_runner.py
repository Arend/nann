import h5py
import numpy as np
import os
import sys

import nann.index as idx
import nann.model as model
import nann.multi_index as midx

VERBOSE = False


def _load_ds():
    try:
        ds = h5py.File("datasets/random-xs-20-euclidean.hdf5", "r")
    except OSError:
        import urllib.request

        if not os.path.exists("datasets"):
            os.makedirs("datasets")
        f = urllib.request.urlretrieve(
            "http://vosmaer.nl/datasets/random-xs-20-euclidean.hdf5",
            "datasets/random-xs-20-euclidean.hdf5",
        )
        ds = h5py.File("datasets/random-xs-20-euclidean.hdf5", "r")
    return ds


def test_multi_index(model_type, nbuckets, f):
    ds = _load_ds()
    X = np.array(ds["train"])
    queries = ds["test"][:10]
    expecteds = ds["neighbors"][:10]

    index = midx.MultiIndex(
        X=X,
        metric="euclidean",
        S=100,
        nbuckets=nbuckets,
        epochs=1,
        kaffpa_path="../KaHIP/deploy/kaffpa",
        kaffpa_preconfig="fast",
        nlayers=3,
        model_type=model_type,
        original_indices=np.arange(len(X)),
        nblocks=1,
        layer_size=128,
    )

    match = 0
    for expected, query in zip(expecteds, queries):
        nbrs = index.query(query, 100, f)
        if VERBOSE:
            print(f"Expected: {expected}")
            print(f"Found: {nbrs}")
        for v in expected:
            if v in nbrs:
                match += 1
    percent_found = match / len(queries)
    print(f"MultiIndex {model_type} found {percent_found}% of expected (f = {f}).")
    if model_type != "kmeans":
        assert percent_found > 50


def test_index(model_type, nbuckets, f):
    ds = _load_ds()
    X = np.array(ds["train"])
    query = ds["test"][0]
    expected = ds["neighbors"][0]

    index = idx.Index(
        X=X,
        metric="euclidean",
        S=100,
        nbuckets=nbuckets,
        epochs=1,
        kaffpa_path="../KaHIP/deploy/kaffpa",
        kaffpa_preconfig="fast",
        model_type=model_type,
        original_indices=np.arange(len(X)),
        nblocks=3,
        layer_size=128,
    )

    nbrs = index.query(query, 100, f)
    if VERBOSE:
        print(f"Expected: {expected}")
        print(f"Found: {nbrs}")
    assert (expected == nbrs).any()
    assert (expected == nbrs).all()


def test_index_batch(model_type, nbuckets, f):
    ds = _load_ds()
    X = np.array(ds["train"])
    query = ds["test"][:10]
    expected = ds["neighbors"][:10]

    index = idx.Index(
        X=X,
        metric="euclidean",
        S=100,
        nbuckets=nbuckets,
        epochs=1,
        kaffpa_path="../KaHIP/deploy/kaffpa",
        kaffpa_preconfig="fast",
        model_type=model_type,
        original_indices=np.arange(len(X)),
    )

    nbrs = index.batch_query(query, 100, f)

    matches = 0
    for res, exp in zip(nbrs, expected):
        for v in res:
            if v in exp:
                matches += 1
    match_percent = matches / len(nbrs)
    print(f"Batch {model_type} found {match_percent}% of expected.")

    if VERBOSE:
        print(f"Expected: {expected}")
        print(f"Found: {nbrs}")


if __name__ == "__main__":
    if len(sys.argv) > 1 and "-v" in sys.argv[1]:
        VERBOSE = True
    for mt in model.get_model_names():
        nbuckets = 3
        f = 0.7
        print(mt, nbuckets, f)
        test_index(mt, nbuckets, f)
        test_index_batch(mt, nbuckets, f)
        test_multi_index(mt, nbuckets, f)
