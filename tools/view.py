import sys

import h5py as h

fname = sys.argv[1]
ds = h.File(fname, "r")
for key in ds.keys():
    print(f"Key: {key}")
    print(f"Shape: {ds[key].shape}")
    print(f"First value: {ds[key][0]}")
    print()
