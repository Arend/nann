import sys

with open(sys.argv[1], "r") as infile:
    graph = []
    for line in infile.readlines()[1:]:
        nns = [int(v) for v in line.split()]
        graph.append(nns)

with open(sys.argv[2], "r") as infile:
    partition = [int(i) for i in infile.readlines()]
    print(len(partition))

minlen, maxlen, minsum, maxsum = 0, 0, 0, 0
maxsumidx = -1
for i, v in enumerate(graph):
    counts = {}
    for nn in v:
        p = partition[nn - 1]
        if p in counts:
            counts[p] += 1
        else:
            counts[p] = 1
    result = []
    for k in counts.keys():
        result.append(counts[k])
    print(sorted(result))
    length = len(result)
    rsum = sum(result)
    if minlen > length or minlen == 0:
        minlen = length
    if maxlen < length:
        maxlen = length
    if minsum > rsum or minsum == 0:
        minsum = rsum
    if maxsum < rsum:
        maxsum = rsum
        maxsumidx = i
    if i == 5581:
        input()
print(minlen, maxlen)
print(minsum, maxsum)
print(maxsumidx)
