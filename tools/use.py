import nann.nann as nann


class Tester:
    def fit(self, X, neighbors, k):
        """X is the training part of a dataset, used to build up the index"""
        self._index = nann.Nann(X, neighbors, k)

    def query(self, v, n):
        ind, dist = self._index.query(v, k=n)
        return ind


t = Tester()
import h5py

ds = h5py.File("datasets/glove-full.hdf5", "r")
X = ds["train"]
neighbors = ds["neighbors"]
t.fit(X, neighbors, 10)
# import nann.extract_training_data as ext

# ext.extract_training_data(X)
