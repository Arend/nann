import click
import h5py
import keras
import numpy as np

from nann.kmeans import KMeans
from nann.neural_lsh import NeuralLSH


@click.command()
@click.option("-d", "--dataset", type=click.Path(exists=True), required=True)
@click.option("-m", "--model", type=click.Path(exists=True), required=True)
def main(dataset, model):
    ds = h5py.File(dataset, "r")
    x_test = ds["X_test"]
    y_test = ds["y_test"]

    try:
        nn = NeuralLSH(1)
        nn.load(model)
        model = nn
    except OSError:
        km = KMeans(1)
        km.load(model)
        model = km

    correct, incorrect = (0, 0)
    i = 0
    for x, y in zip(x_test, y_test):
        i += 1
        if i % 500 == 0:
            print("predicted 500")
        distribution = model.predict_one(x)
        y_ = np.argmax(distribution)
        if (y == y_).all():
            correct += 1
        else:
            incorrect += 1

    print(f"Correct:\t{correct}")
    print(f"Incorrect:\t{incorrect}")


if __name__ == "__main__":
    main()
