import os
import sys

import h5py

dataset = sys.argv[1]
fn, ext = os.path.splitext(dataset)
new_ds = fn + "-small" + ext
assert dataset != new_ds

ds = h5py.File(dataset, "r")
xte = ds["X_test"]
yte = ds["y_test"]
xtr = ds["X_train"]
ytr = ds["y_train"]

with h5py.File(new_ds, "w") as outfile:
    outfile.create_dataset("X_test", data=xte)
    outfile.create_dataset("y_test", data=yte)
    outfile.create_dataset("X_train", data=xtr)
    outfile.create_dataset("y_train", data=ytr)
