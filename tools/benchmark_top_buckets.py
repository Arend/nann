import sys
import time

import numpy as np

from nann.kmeans import KMeans


def _query(top_buckets, f, distribution, n):
    t_total = np.empty(n)
    for i in range(n):
        t0 = time.time()
        top_buckets(distribution, f)
        t1 = time.time()
        t_total[i] = t1 - t0
    return t_total


def run_test(dim, n):
    pp = KMeans(100)
    f = 0.9
    distribution = np.random.rand(dim, 100)
    distribution = distribution / np.sum(
        distribution, axis=1, keepdims=True
    )  # normalize
    n = 100
    single = np.zeros(n)
    for d in range(dim):
        single += _query(pp.get_top_buckets, f, distribution[d], n)
    batch = _query(pp.batch_get_top_buckets, f, distribution, n)

    print(f"Single query average runtime: {sum(single)/n*1000} ms")
    print(f"Batch query average runtime:  {sum(batch)/n*1000} ms")


if __name__ == "__main__":
    dim = int(sys.argv[1])
    run_test(dim, 1000)
