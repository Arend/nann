import sys

with open(sys.argv[1], "r") as infile:
    partition = [int(i) for i in infile.readlines()]

counts = {}
for v in partition:
    if v in counts:
        counts[v] += 1
    else:
        counts[v] = 1

sortedkeys = sorted(counts.keys())
for key in sortedkeys:
    print(f"{key}: {counts[key]}")
