black==18.9b0
pytest==3.6.2
pyspelling==2.2.5
mypy==0.711
pytest-cov==2.7.1
urllib3
