from setuptools import setup, find_packages

setup(
    name="nann",
    version="0.1",
    packages=find_packages(),
    install_requires=["h5py", "Click", "Keras", "numba", "scikit-learn", "tensorflow"],
    entry_points="""[console_scripts]
        dataset=nann.build_dataset:main
        model=nann.model:main
        nann=nann.nann:main
        evaluate=nann.evaluate:cli
        """,
)
